package application;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class AlerBox {

	public static void display(String title, String message) {
		final Stage window=new Stage();
		
		//adding method which stops from doing anything to other window before 
		//finishing some action on the current one
		window.initModality(Modality.APPLICATION_MODAL);
		
		window.setTitle(title);
		//window.setMinWidth(400);
		//window.setMinHeight(200);
		
		Label label=new Label();
		label.setText(message);
		//*************************
		label.setStyle("-fx-font-size: 15px; -fx-font-weight:bold; -fx-text-fill: gray");
		
		Button button=new Button("Close");
		//**************************
		button.setStyle("-fx-background-color:tomato; -fx-text-fill:white; -fx-font-size: 13px; -fx-font-weight: bold");
		//********************
		button.setPadding(new Insets(5,10,5,10));
		button.setOnAction(
				new EventHandler<ActionEvent>() {
					
					public void handle(ActionEvent arg0) {
						window.close();
					}
				}
				/*e->window.close()*/);
		
		VBox layout=new VBox(10);
		layout.getChildren().addAll(label, button);
		//*******************
		layout.setSpacing(15);
		//*****************
		layout.setStyle("-fx-background-color:#F5DEB3");
		layout.setAlignment(Pos.CENTER);
		
		Scene scene=new Scene(layout, 500, 150);
		window.setScene(scene);
		//************************
		window.centerOnScreen();
		window.showAndWait();
	}
}
