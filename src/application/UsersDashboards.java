package application;

import java.util.List;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import rs.laniteo.todoapp.dao.DashboardDAO;
import rs.laniteo.todoapp.dao.UserDAO;
import rs.laniteo.todoapp.model.Dashboard;
import rs.laniteo.todoapp.model.User;

public class UsersDashboards {
	
	static Stage windowMain=new Stage();
	public static Scene displayDashboards(User user) {
		//window=new Stage();
		VBox layout=new VBox(10);
		//***************OVO NEMOJ
		layout.setStyle("-fx-background-color:lightblue");
		//**********************IZMJENA		
		layout.setPadding(new Insets(15,30,15,30));
		//**********************
		layout.setSpacing(15);
		ScrollPane sp=new ScrollPane();
		//*********************
		sp.setFitToHeight(true);
		//**************************
		sp.setFitToWidth(true);
		sp.setContent(layout);
		Scene scene=new Scene(sp, 400, 500,  Color.BLUE);
		//scene.getRoot().setPa
		Button button=new Button("Create dashboard.");
		//**********************
		button.setStyle("-fx-background-color:#f7d06e; -fx-text-fill:white; -fx-font-size: 15px; -fx-font-weight: bold");
		//******************************
		button.setPadding(new Insets(10,10,10,10));
		button.setOnAction(
				new EventHandler<ActionEvent>() {
					
					public void handle(ActionEvent arg0) {
						createDashboard();
					}
				}
				/*e->{
			createDashboard();
		}*/);
		
		windowMain.setTitle("ToDoApp");
		//windowMain.setMinWidth(1000);
		//windowMain.setMinHeight(500);
		Label label=new Label();
		label.setText("Welcome, "+user.getUsername());
		//*************************
		label.setStyle("-fx-text-fill:#808080; -fx-font-size: 15px; -fx-font-weight: bold;");
		layout.getChildren().add(label);
		
		
		layout.getChildren().addAll(button);
		windowMain.setScene(scene);
		
		UserDAO userDao=new UserDAO();
		List<Dashboard> dashboards=userDao.getUsersDashboards(user);
		if(dashboards.size()==0) {
			Label label2=new Label();
			label2.setText("You do not have any dashboards yet.");
			layout.getChildren().add(label2);
		}
		else {
			VBox hbox1=new VBox();
			//*********************
			hbox1.setSpacing(15);
			//******************
			hbox1.setMinWidth(350);
			for(final Dashboard d:dashboards) {
				HBox dash=new HBox();
				//****************
				dash.setSpacing(5);
				Button button1=new Button(d.getTitle());
				//*****************************
				button1.setStyle("-fx-background-color:white; -fx-text-fill:lightblue; -fx-font-size: 15px; -fx-font-weight: bold;");
				//******************
				button1.setMinWidth(235);
				//******************
				button1.setMaxWidth(235);
				dash.getChildren().add(button1);
				button1.setOnAction(
						new EventHandler<ActionEvent>() {
							public void handle(ActionEvent e){
								windowMain.setScene(UsersTaskStates.displayTaskStates(d));
								//***************************
								windowMain.centerOnScreen();
								//****************************
								windowMain.setTitle(d.getTitle());
							}
						}
						/*e->{
					windowMain.setScene(UsersTaskStates.displayTaskStates(d));
					windowMain.setTitle(d.getTitle());
				}*/);
				//********************IZMJENA
				Button buttonUpdate=new Button("���");
				//**********************
				buttonUpdate.setStyle("-fx-font-size: 15px; -fx-padding: 5px; -fx-font-weight: bold; -fx-text-fill: white; -fx-background-color: transparent");
				//********************
				buttonUpdate.setMinWidth(40);
				dash.getChildren().add(buttonUpdate);
				buttonUpdate.setOnAction(
						new EventHandler<ActionEvent>() {
							public void handle(ActionEvent e){
								updateDashboard(d);
							}
						}
						/*e->updateDashboard(d)*/);
				
				Button buttonDelete=new Button("X");
				//**********************
				buttonDelete.setStyle("-fx-font-size: 15px; -fx-padding: 5px; -fx-font-weight: bold; -fx-text-fill: white; -fx-background-color: transparent");
				//*********************
				buttonDelete.setMinWidth(40);
				dash.getChildren().add(buttonDelete);
				buttonDelete.setOnAction(
						new EventHandler<ActionEvent>() {
							public void handle(ActionEvent e){
								deleteDashboard(d);
							}
						}
						/*e->deleteDashboard(d)*/
						);
				
				hbox1.getChildren().add(dash);
			}
			layout.getChildren().add(hbox1);
			
		}
		windowMain.centerOnScreen();
		windowMain.show();
		return scene;
	}
	
	public static void createDashboard() {
		final Stage window=new Stage();
		window.initModality(Modality.APPLICATION_MODAL);
		//*****************
		window.setTitle("New dashboard");
		//*********************
		window.setMinHeight(200);
		//********************
		window.setMinWidth(300);
		VBox vbox=new VBox();
		//********************
		vbox.setStyle("-fx-background-color:lightblue");
		//*************************
		vbox.setSpacing(15);
		//**********************
		vbox.setPadding(new Insets(15,30,15,30));
		Label labelTitle=new Label("Title: ");
		//*******************
		labelTitle.setStyle("-fx-font-size: 15px");
		final TextField title=new TextField();
		title.setPromptText("title");
		Button save=new Button("Save");
		//***********************
		save.setStyle("-fx-background-color:dodgerblue; -fx-text-fill:white; -fx-font-size: 13px; -fx-font-weight: bold");
		save.setOnAction(
				new EventHandler<ActionEvent>() {
					public void handle(ActionEvent e){
						if(title.getText().trim().equals("") || title.getText().length()>50) {
							AlerBox.display("", "Dashboard title can not be empty or contain more than 50 characters.");
							return;
						}
						Dashboard dashboard=new Dashboard();
						dashboard.setTitle(title.getText());
						dashboard.setUser(Login.loggedUser);
						DashboardDAO dashboardDao=new DashboardDAO();
						dashboardDao.addDashboard(dashboard);
						//scene=new Scene(displayDashboards(Login.loggedUser));
						
						//Scene scene2=displayDashboards(Login.loggedUser);
						windowMain.setScene(displayDashboards(Login.loggedUser));
						window.close();
					}
				}
				/*e->{
			if(title.getText().trim().equals("") || title.getText().length()>50) {
				AlerBox.display("", "Dashboard title can not be empty or contain more than 50 characters.");
				return;
			}
			Dashboard dashboard=new Dashboard();
			dashboard.setTitle(title.getText());
			dashboard.setUser(Login.loggedUser);
			DashboardDAO dashboardDao=new DashboardDAO();
			dashboardDao.addDashboard(dashboard);
			//scene=new Scene(displayDashboards(Login.loggedUser));
			
			//Scene scene2=displayDashboards(Login.loggedUser);
			windowMain.setScene(displayDashboards(Login.loggedUser));
			window.close();
		}*/);
		vbox.getChildren().addAll(labelTitle, title, save);
		Scene scene=new Scene(vbox, 200, 75);
		window.setScene(scene);
		//******************
		window.centerOnScreen();
		window.showAndWait();
	}
	
	public static void updateDashboard(final Dashboard dashboard) {
		final Stage window=new Stage();
		window.initModality(Modality.APPLICATION_MODAL);
		//*****************
		window.setTitle("Update dashboard");
		//*********************
		window.setMinHeight(200);
		//********************
		window.setMinWidth(300);
		VBox vbox=new VBox();
		//********************
		vbox.setStyle("-fx-background-color:lightblue");
		//*************************
		vbox.setSpacing(15);
		//**********************
		vbox.setPadding(new Insets(15,30,15,30));
		///************************DODDAT LABEL
		Label labelTitle=new Label("Title: ");
		//*******************
		labelTitle.setStyle("-fx-font-size: 15px");
		final TextField title=new TextField(dashboard.getTitle());
		Button save=new Button("Save");
		//************************
		save.setStyle("-fx-background-color:dodgerblue; -fx-text-fill:white; -fx-font-size: 13px; -fx-font-weight: bold");
		save.setOnAction(
				new EventHandler<ActionEvent>() {
					public void handle(ActionEvent e){
						if(title.getText().trim().equals("") || title.getText().length()>50) {
							AlerBox.display("", "Dashboard title can not be empty or contain more than 50 characters.");
							return;
						}
						dashboard.setTitle(title.getText());
						DashboardDAO dashboardDao=new DashboardDAO();
						dashboardDao.updateDashboard(dashboard);
						window.close();
						windowMain.setScene(displayDashboards(Login.loggedUser));
					}
				}
				/*e->{
			if(title.getText().trim().equals("") || title.getText().length()>50) {
				AlerBox.display("", "Dashboard title can not be empty or contain more than 50 characters.");
				return;
			}
			dashboard.setTitle(title.getText());
			DashboardDAO dashboardDao=new DashboardDAO();
			dashboardDao.updateDashboard(dashboard);
			window.close();
			windowMain.setScene(displayDashboards(Login.loggedUser));
			//scene=new Scene(displayDashboards(Login.loggedUser));
		}*/);
		//*************DODAT LABELTITLE
		vbox.getChildren().addAll(labelTitle,title, save);
		Scene scene=new Scene(vbox, 200, 75);
		window.setScene(scene);
		//*******************
		window.centerOnScreen();
		window.showAndWait();
	}
	
	public static void deleteDashboard(Dashboard dashboard) {
		DashboardDAO dashboardDao=new DashboardDAO();
		dashboardDao.deleteDashboard(dashboard.getId());
		windowMain.setScene(displayDashboards(Login.loggedUser));
	}
}
