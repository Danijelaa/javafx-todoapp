package application;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.Background;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import rs.laniteo.todoapp.dao.UserDAO;
import rs.laniteo.todoapp.model.User;
import rs.laniteo.todoapp.utils.CreateConnection;

public class Login  extends Application{
	public static User loggedUser=null;
	static Stage window;
	static Scene scene;
	GridPane grid;
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		CreateConnection.initializeDatabase();
		
		window=primaryStage;
		
		grid=new GridPane();
		//*************IZMJENA
		grid.setPadding(new Insets(15,30,15,30));
		grid.setVgap(8);
		grid.setHgap(10);
		//**********************
		grid.setStyle("-fx-background-color:lightblue");
		//grid.setBackground(new Background(new BackgroundFill(Color.DARKGREEN, CornerRadii.EMPTY, Insets.EMPTY)));
		Label labelName=new Label("Username:");
		//*****************
		labelName.setStyle("-fx-font-size: 15px");
		GridPane.setConstraints(labelName, 0, 0);
		//input
		final TextField inputName=new TextField();
		inputName.setPromptText("username");
		GridPane.setConstraints(inputName, 1, 0);
		
		Label labelPass=new Label("Password:");
		//*********************
		labelPass.setStyle("-fx-font-size: 15px");
		GridPane.setConstraints(labelPass, 0, 1);

		final PasswordField inputPassword=new PasswordField();
		//grey text
		inputPassword.setPromptText("password");
		GridPane.setConstraints(inputPassword, 1, 1);
		
		Button loginButton=new Button("Login");
		//**********************
		loginButton.setStyle("-fx-background-color:dodgerblue; -fx-text-fill:white; -fx-font-size: 13px; -fx-font-weight: bold");
		loginButton.setOnAction(new EventHandler<ActionEvent>() {
			
			public void handle(ActionEvent arg0) {
				boolean validated=validate(inputName, inputPassword);
				if(validated) {
					UserDAO userDao=new UserDAO();
					User user=userDao.login(inputName.getText(), inputPassword.getText());
					if(user!=null) {
						loggedUser=user;
						UsersDashboards.displayDashboards(user);
						window.close();
					}
					else {
						AlerBox.display("User not found", "No user with given name and password.");
						//message.setText("No user with given username and password.");
					}
				}
				
			}
		}/*e->{/*e->{
			boolean validated=validate(inputName, inputPassword);
			if(validated) {
				UserDAO userDao=new UserDAO();
				User user=userDao.login(inputName.getText(), inputPassword.getText());
				if(user!=null) {
					loggedUser=user;
					UsersDashboards.displayDashboards(user);
					window.close();
				}
				else {
					AlerBox.display("User not found", "No user with given name and password.");
					//message.setText("No user with given username and password.");
				}
			}
		}*/);
		GridPane.setConstraints(loginButton, 1, 2);
		
		Label noaccount=new Label("You do not have account? ");
		GridPane.setConstraints(noaccount, 0, 4);
		Button registerButton=new Button("Register");
		//*************************
		registerButton.setStyle("-fx-background-color:#f7d06e; -fx-text-fill:white; -fx-font-size: 13px; -fx-font-weight: bold");
		registerButton.setOnAction(
				new EventHandler<ActionEvent>() {
					public void handle(ActionEvent e){
						window.setScene(Register.register());
						window.setTitle("Registration");
					}
				}/*e->{/*e->{/*e->{/*e->{/*e->{/*e->{/*e->{/*e->{
			window.setScene(Register.register());
			window.setTitle("Registration");
		}*/);
		GridPane.setConstraints(registerButton, 1, 4);
		
		GridPane.setConstraints(labelPass, 0, 1);
		grid.getChildren().addAll(labelName, inputName, labelPass, inputPassword, loginButton, noaccount, registerButton);
		scene=new Scene(grid, 500, 200);
		
		window.setScene(scene);
		//*********************************
		window.centerOnScreen();
		//****************************
		window.setTitle("Login");
		window.show();
	}
	private boolean validate(TextField inputName, TextField inputPassword) {
		if(inputName.getText().trim().equals("") || inputPassword.getText().trim().equals("")) {
			AlerBox.display("Emtty felds", "Both fields must be filled.");
			return false;
		}
		return true;
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
