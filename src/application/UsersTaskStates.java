package application;

import java.nio.file.attribute.UserDefinedFileAttributeView;
import java.util.List;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import rs.laniteo.todoapp.dao.DashboardDAO;
import rs.laniteo.todoapp.dao.TaskDAO;
import rs.laniteo.todoapp.dao.TaskStateDAO;
import rs.laniteo.todoapp.model.Dashboard;
import rs.laniteo.todoapp.model.Task;
import rs.laniteo.todoapp.model.TaskState;

public class UsersTaskStates {
	//****************
	static VBox vbox;
	public static Scene displayTaskStates(final Dashboard dashboard) {
		/*VBox */vbox=new VBox(10);
		//**********************IZMJENA		
		vbox.setPadding(new Insets(15,30,15,30));
		//******************************
		vbox.setStyle("-fx-background-color:lightblue");
		//vbox.setSpacing(10);
		ScrollPane sp=new ScrollPane();
		sp.setContent(vbox);
		//*********************
		sp.setFitToHeight(true);
		//**************************
		sp.setFitToWidth(true);
		Button back=new Button("My dashboards");
		//********************
		back.setStyle("-fx-background-color:white; -fx-text-fill:#4169E1; -fx-font-size: 15px; -fx-font-weight: bold;");
		back.setOnAction(
				new EventHandler<ActionEvent>() {
					public void handle(ActionEvent e){
						UsersDashboards.windowMain.setScene(UsersDashboards.displayDashboards(Login.loggedUser));
					}
				}
				/*e->UsersDashboards.windowMain.setScene(UsersDashboards.displayDashboards(Login.loggedUser))*/);
		Button button=new Button("Create task state");
		button.setOnAction(
				new EventHandler<ActionEvent>() {
					public void handle(ActionEvent e){
						addTaskState(dashboard);
					}
				}
				/*e->addTaskState(dashboard)*/);
		vbox.getChildren().addAll(back, button);
		
		HBox hbox=new HBox();
		//hbox.setSpacing(10);
		DashboardDAO dashboardDao=new DashboardDAO();
		final List<TaskState> taskstates=dashboardDao.getTaskStates(dashboard);
		HBox hbox1=new HBox();
		if(taskstates.size()==0) {
			Label label=new Label("You do not have any task states yet.");
			hbox.getChildren().add(label);
		}
		else {
			//*****************POMJEREN GORE HBOX1
			hbox1.setSpacing(12);
			//hbox1.setMinWidth(500);
			for(final TaskState ts:taskstates) {
				VBox hb=new VBox();
				//*****************ITZMJENA
				hb.setStyle(/*"-fx-border-style: solid;-fx-border-width: 1;-fx-border-color: silver;*/"  -fx-background-radius :5px; "
						+ "-fx-background-color:white");
				hb.setPadding(new Insets(10,10,10,10));
				hb.setSpacing(12);
				//******************DODATA JOS JEDNA DIMENZIJA
				hb.setMinWidth(350);
				hb.setMaxWidth(350);
				HBox hbbb=new HBox();
				Label title=new Label(ts.getTitle());
				//title.setStyle("-fx-border-style: solid;-fx-border-width: 1;-fx-border-color: silver; -fx-border-radius:4pt; ");
				//*************************
				title.setMinWidth(245);
				//****************
				title.setMaxWidth(245);
				title.setStyle("-fx-text-fill:#808080; -fx-font-size: 15px; -fx-font-weight: bold;");
				//****************IZMJENA
				Button updateTaskstate=new Button("···");
				//******************
				updateTaskstate.setMinWidth(35);
				//*******************
				updateTaskstate.setStyle("-fx-font-size: 13px; -fx-padding: 5px; -fx-background-color: white; -fx-text-fill:black");
				updateTaskstate.setOnAction(
						new EventHandler<ActionEvent>() {
							public void handle(ActionEvent e){
								displayTaskState(ts, dashboard);
							}
						}
						/*e->{
					displayTaskState(ts, dashboard);
				}*/);
				Button deleteTaskstate=new Button("X");
				//*****************
				deleteTaskstate.setMinWidth(35);
				//*************************
				deleteTaskstate.setStyle("-fx-font-size: 13px; -fx-padding: 5px; -fx-background-color: white; -fx-text-fill:#808080; -fx-font-weight: bold");
				deleteTaskstate.setOnAction(
						new EventHandler<ActionEvent>() {
							public void handle(ActionEvent e){
								deleteTaskState(ts.getId(), dashboard);
							}
						}
						/*e->{
					deleteTaskState(ts.getId(), dashboard);
				
				}*/);
				//**************************
				hbbb.setSpacing(5);
				hbbb.getChildren().addAll(title, updateTaskstate, deleteTaskstate);
				//hbbb.setStyle("-fx-bottom-border-style: solid; -fx-bottom-border-width: 1;-fx-bottom-border-color: silver;");
				hb.getChildren().addAll(hbbb);
				TaskStateDAO tsD=new TaskStateDAO();
				List<Task> tasks=tsD.getTasks(ts);
				for(final Task t:tasks) {
					HBox hbb=new HBox();
					final Button bt=new Button(t.getTitle());
					//********************************
					bt.setStyle("-fx-background-color:#4169E1; -fx-text-fill:white; -fx-font-size: 15px;");
					//*********************
					bt.setMinWidth(285);
					//*******************
					bt.setMaxWidth(285);
					bt.setOnAction(
							new EventHandler<ActionEvent>() {
								public void handle(ActionEvent e){
									displayTask(t, taskstates, dashboard);
									System.out.println(bt.getWidth());
								}
							}
							/*e->displayTask(t, taskstates, dashboard)*/);
					bt.setPadding(new Insets(10,10,10,10));
					//bt.setPadding(new Insets(10,10,10,10));
					//bt.setOnDragDropped(e->System.out.println("Jei"));
					/*Button o=new Button("☼");
					//************************
					//o.setStyle("-fx-background-color:#FF6347; -fx-text-fill:white; -fx-font-size: 15px; -fx-font-weight: bold");
					o.setPadding(new Insets(10,10,10,10));
					o.setOnAction(
							new EventHandler<ActionEvent>() {
								public void handle(ActionEvent e){
									displayTask(t, taskstates, dashboard);
								}
							}
							e->displayTask(t, taskstates, dashboard));
					*/
					Button delete=new Button("X");
					//***********************
					delete.setStyle("-fx-background-color:white; -fx-text-fill:#4169E1; -fx-font-size: 15px; -fx-font-weight: bold;");
					delete.setMinWidth(35);
					delete.setPadding(new Insets(10,10,10,10));
					delete.setOnAction(
							new EventHandler<ActionEvent>() {
								public void handle(ActionEvent e){
									deleteTask(t.getId(), dashboard);
								}
							}
							/*e->{
						deleteTask(t.getId(), dashboard);
					}*/);
					//hbb.getChildren().addAll(bt,o, delete);
					hbb.getChildren().addAll(bt,delete);
					hbb.setSpacing(5);
					hb.getChildren().add(hbb);
				}
				Button addTask=new Button("Add task");
				//*********************
				addTask.setStyle("-fx-background-color:#f7d06e; -fx-text-fill:white; -fx-font-size: 13px; -fx-font-weight: bold");
				//////////////////NE ZNAM DA L JE I OVO DODATO
				addTask.setPadding(new Insets(5,10,5,10));
				addTask.setOnAction(
						new EventHandler<ActionEvent>() {
							public void handle(ActionEvent e){
								createTask(ts, dashboard);
							}
						}
						/*e->createTask(ts, dashboard)*/);
				hb.getChildren().add(addTask);
				
				hbox1.getChildren().addAll(hb);
			}
		}
		//********************
		Button addingTaskState=new Button("Add task state");
		//********************************
		addingTaskState.setOnAction(
				new EventHandler<ActionEvent>() {
					public void handle(ActionEvent e){
						addTaskState(dashboard);
					}
				});
		//************************
		addingTaskState.setStyle("-fx-background-color: #B0C4DE;  -fx-text-fill:white; -fx-font-size: 15px; -fx-font-weight: bold");
		//******************************
		addingTaskState.setMinWidth(350);
		//**********************
		addingTaskState.setMaxWidth(350);
		//******************************
		addingTaskState.setPadding(new Insets(10,10,10,10));
		//**************************
		hbox1.getChildren().addAll(addingTaskState);
		vbox.getChildren().addAll(hbox1);
		
		vbox.getChildren().add(hbox);
		Scene scene=new Scene(sp, 1000, 500);
		return scene;
	}
	
	public static void deleteTask(Integer id, Dashboard dashboard) {
		TaskDAO taskDao=new TaskDAO();
		taskDao.deleteTask(id);
		UsersDashboards.windowMain.setScene(displayTaskStates(dashboard));
	}
	
	public static void deleteTaskState(Integer id, Dashboard dashboard) {
		TaskStateDAO taskStateDao=new TaskStateDAO();
		taskStateDao.deleteTaskState(id);
		UsersDashboards.windowMain.setScene(displayTaskStates(dashboard));
	}
	public static void displayTaskState(final TaskState taskState, final Dashboard dashboard) {
		final Stage window=new Stage();
		window.initModality(Modality.APPLICATION_MODAL);
		VBox vbox=new VBox();
		//****************
		vbox.setSpacing(15);
		//**********************
		vbox.setStyle("-fx-background-color:lightblue");
		//*********************
		vbox.setPadding(new Insets(15,30,15,30));
		//*********************
		window.setMinHeight(200);
		//********************
		window.setMinWidth(300);
		Label titleLabel=new Label("Title: ");
		//*************************
		titleLabel.setStyle("-fx-font-size: 15px");
		final TextField title=new TextField(taskState.getTitle());
		Button save=new Button("Save");
		//*****************************
		save.setStyle("-fx-background-color:dodgerblue; -fx-text-fill:white; -fx-font-size: 13px; -fx-font-weight: bold");

		save.setOnAction(
				new EventHandler<ActionEvent>() {
					public void handle(ActionEvent e){
						if(title.getText().trim().equals("") || title.getText().length()>50) {
							AlerBox.display("", "Task title can not be empty or contain more than 50 characters.");
							return;
						}
						taskState.setTitle(title.getText());
						TaskStateDAO taskStateDao=new TaskStateDAO();
						taskStateDao.updateTaskState(taskState);
						window.close();
						UsersDashboards.windowMain.setScene(displayTaskStates(dashboard));
					}
				}
				/*e->{
			if(title.getText().trim().equals("") || title.getText().length()>50) {
				AlerBox.display("", "Task title can not be empty or contain more than 50 characters.");
				return;
			}
			taskState.setTitle(title.getText());
			TaskStateDAO taskStateDao=new TaskStateDAO();
			taskStateDao.updateTaskState(taskState);
			window.close();
			UsersDashboards.windowMain.setScene(displayTaskStates(dashboard));
		}*/);
		vbox.getChildren().addAll(titleLabel, title, save);
		Scene scene=new Scene(vbox);
		window.setScene(scene);
		window.showAndWait();
	}
	
	public static void createTask(final TaskState taskState, final Dashboard dashboard) {
		final Stage window=new Stage();
		//*****************
		window.setTitle("Create task");
		window.initModality(Modality.APPLICATION_MODAL);
		VBox vbox=new VBox();
		//****************
		vbox.setSpacing(15);
		Label titleLabel=new Label("Title: ");
		//***********************
		titleLabel.setStyle("-fx-font-size: 15px");
		final TextField title=new TextField();
		title.setPromptText("title");
		Label descLabel=new Label("Description: ");
		//***********************
		descLabel.setStyle("-fx-font-size: 15px");
		final TextArea description=new TextArea();
		description.setPromptText("Description...");
		Button save=new Button("Save");
		//******************************
		save.setStyle("-fx-background-color:dodgerblue; -fx-text-fill:white; -fx-font-size: 13px; -fx-font-weight: bold");
		save.setOnAction(
				new EventHandler<ActionEvent>() {
					public void handle(ActionEvent e){
						if(title.getText().trim().equals("") || title.getText().length()>50) {
							AlerBox.display("", "Task title can not be empty or contain more than 50 characters.");
							return;
						}
						if(description.getText().length()>300) {
							AlerBox.display("", "Task description can not contain more than 300 characters.");
							return;
						}
						TaskDAO taskDao=new TaskDAO();
						Task task=new Task();
						task.setDescription(description.getText());
						task.setTaskState(taskState);
						task.setTitle(title.getText());
						taskDao.addTask(task);
						window.close();
						UsersDashboards.windowMain.setScene(displayTaskStates(dashboard));
					}
				}
				/*e->{
			if(title.getText().trim().equals("") || title.getText().length()>50) {
				AlerBox.display("", "Task title can not be empty or contain more than 50 characters.");
				return;
			}
			if(description.getText().length()>300) {
				AlerBox.display("", "Task description can not contain more than 300 characters.");
				return;
			}
			TaskDAO taskDao=new TaskDAO();
			Task task=new Task();
			task.setDescription(description.getText());
			task.setTaskState(taskState);
			task.setTitle(title.getText());
			taskDao.addTask(task);
			window.close();
			UsersDashboards.windowMain.setScene(displayTaskStates(dashboard));
		}*/);
		vbox.getChildren().addAll(titleLabel, title, descLabel, description, save);
		Scene scene=new Scene(vbox);
		//**********************
		vbox.setPadding(new Insets(15,30,15,30));
		//******************
		vbox.setStyle("-fx-background-color:lightblue");
		window.setScene(scene);
		//******************
		window.centerOnScreen();
		window.showAndWait();
	}
	public static void addTaskState(final Dashboard dashboard) {
		final Stage window=new Stage();
		window.initModality(Modality.APPLICATION_MODAL);
		//******************
		window.setTitle("Create task state");
		
		VBox vbox=new VBox();
		//****************
		vbox.setSpacing(15);
		//**********************
		vbox.setStyle("-fx-background-color:lightblue");
		//*********************
		vbox.setPadding(new Insets(15,30,15,30));
		//*********************
		window.setMinHeight(200);
		//********************
		window.setMinWidth(300);
		Label titleLabel=new Label("Title: ");
		//******************************
		titleLabel.setStyle("-fx-font-size: 15px");
		final TextField title=new TextField();
		title.setPromptText("title");
		Button save=new Button("Save");
		//*********************************
		save.setStyle("-fx-background-color:dodgerblue; -fx-text-fill:white; -fx-font-size: 13px; -fx-font-weight: bold");
		save.setOnAction(
				new EventHandler<ActionEvent>() {
					public void handle(ActionEvent e){
						if(title.getText().trim().equals("") || title.getText().length()>50) {
							AlerBox.display("", "Task state title can not be empty or contain more than 50 characters.");
							return;
						}
						TaskState taskState=new TaskState();
						taskState.setDashboard(dashboard);
						taskState.setTitle(title.getText());
						TaskStateDAO taskStateDao=new TaskStateDAO();
						taskStateDao.addTaskState(taskState);
						window.close();
						UsersDashboards.windowMain.setScene(displayTaskStates(dashboard));
					}
				}
				/*e->{
			if(title.getText().trim().equals("") || title.getText().length()>50) {
				AlerBox.display("", "Task state title can not be empty or contain more than 50 characters.");
				return;
			}
			TaskState taskState=new TaskState();
			taskState.setDashboard(dashboard);
			taskState.setTitle(title.getText());
			TaskStateDAO taskStateDao=new TaskStateDAO();
			taskStateDao.addTaskState(taskState);
			window.close();
			UsersDashboards.windowMain.setScene(displayTaskStates(dashboard));
		}*/);
		vbox.getChildren().addAll(titleLabel, title, save);
		Scene scene=new Scene(vbox, 200, 75);
		//**************************
		window.centerOnScreen();
		window.setScene(scene);
		window.showAndWait();
	}
	
	public static void displayTask(final Task task, List<TaskState> taskstates, final Dashboard dashboard) {
		final Stage window=new Stage();
		window.initModality(Modality.APPLICATION_MODAL);
		VBox vbox=new VBox();
		//****************
		vbox.setSpacing(15);
		//**********************
		vbox.setStyle("-fx-background-color:lightblue");
		//*********************
		vbox.setPadding(new Insets(15,30,15,30));
		Label titleLabel=new Label("Title: ");
		//*********************
		titleLabel.setStyle("-fx-font-size: 15px");
		final TextField label=new TextField(task.getTitle());
		Label descLabel=new Label("Description: ");
		//***************************
		descLabel.setStyle("-fx-font-size: 15px");
		final TextArea tf=new TextArea(task.getDescription());
		HBox hbox=new HBox();
		Label ts=new Label("Task state: ");
		//*********************************
		ts.setStyle("-fx-font-size: 15px");
		final ChoiceBox<TaskState> taskStatesChose=new ChoiceBox<>();
		taskStatesChose.setValue(task.getTaskState());
		taskStatesChose.getItems().addAll(taskstates);
		//*****************************
		taskStatesChose.setStyle("-fx-background-color:white");
		hbox.getChildren().addAll(ts, taskStatesChose);
		//********************
		hbox.setSpacing(15);
		Button save=new Button("Save");
		//*********************************
		save.setStyle("-fx-background-color:dodgerblue; -fx-text-fill:white; -fx-font-size: 13px; -fx-font-weight: bold");
		save.setOnAction(
				new EventHandler<ActionEvent>() {
					public void handle(ActionEvent e){
						if(label.getText().trim().equals("") || label.getText().length()>50) {
							AlerBox.display("", "Task title can not be empty or contain more than 50 characters.");
							return;
						}
						if(tf.getText().length()>300) {
							AlerBox.display("", "Task description can not contain more than 300 characters.");
							return;
						}
						TaskDAO taskDao=new TaskDAO();
						task.setDescription(tf.getText());
						task.setTitle(label.getText());
						task.setTaskState(taskStatesChose.getValue());
						taskDao.updateTask(task);
						UsersDashboards.windowMain.setScene(displayTaskStates(dashboard));
						window.close();
					}
				}
				/*e->{
			if(label.getText().trim().equals("") || label.getText().length()>50) {
				AlerBox.display("", "Task title can not be empty or contain more than 50 characters.");
				return;
			}
			if(tf.getText().length()>300) {
				AlerBox.display("", "Task description can not contain more than 300 characters.");
				return;
			}
			TaskDAO taskDao=new TaskDAO();
			task.setDescription(tf.getText());
			task.setTitle(label.getText());
			task.setTaskState(taskStatesChose.getValue());
			taskDao.updateTask(task);
			UsersDashboards.windowMain.setScene(displayTaskStates(dashboard));
			window.close();
		}*/);
		
		vbox.getChildren().addAll(titleLabel, label, descLabel, tf, hbox, save);
		Scene scene=new Scene(vbox);
		window.setScene(scene);
		//***************************
		window.centerOnScreen();
		window.showAndWait();
	}
}
