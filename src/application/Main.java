package application;


import java.sql.Connection;
import java.util.List;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.stage.Stage;
import rs.laniteo.todoapp.dao.DashboardDAO;
import rs.laniteo.todoapp.dao.UserDAO;
import rs.laniteo.todoapp.model.Dashboard;
import rs.laniteo.todoapp.model.TaskState;
import rs.laniteo.todoapp.model.User;
import rs.laniteo.todoapp.utils.CreateConnection;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

public class Main extends Application  /*implements EventHandler<ActionEvent>*/{
	
	Stage window;
	Scene scene;
	TableView<TaskState> table;
	@Override
	public void start(Stage primaryStage){
		CreateConnection.initializeDatabase();
		
		window=primaryStage;
		/*TableColumn<TaskState, String> titleColumn=new TableColumn<>("Title");
		titleColumn.setCellValueFactory(new PropertyValueFactory<>("title"));
		
		TableColumn<TaskState, Integer> idColumn=new TableColumn<>("ID");
		idColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
		table=new TableView<>();
		table.setItems(getTaskStates());
		table.getColumns().addAll(idColumn, titleColumn);
		
		VBox vbox=new VBox();
		vbox.getChildren().add(table);*/
		/*GridPane grid=new GridPane();
		grid.setPadding(new Insets(10,10,10,10));
		grid.setVgap(8);
		grid.setHgap(10);
		
		Label labelName=new Label("Username:");
		GridPane.setConstraints(labelName, 0, 0);
		//input
		TextField inputName=new TextField();
		inputName.setPromptText("username");
		//inputName.setOnAction(e->inputName.setText(""));
		//node name, column position, row position
		GridPane.setConstraints(inputName, 1, 0);
		
		
		Label labelPass=new Label("Password:");
		GridPane.setConstraints(labelPass, 0, 1);

		TextField inputPassword=new TextField();
		//grey text
		inputPassword.setPromptText("password");
		//inputPassword.setOnAction(e->inputPassword.setText(""));
		GridPane.setConstraints(inputPassword, 1, 1);
		
		Button loginButton=new Button("Login");
		loginButton.setOnAction(e->{
			boolean validated=validate(inputName, inputPassword);
			if(validated) {
				UserDAO userDao=new UserDAO();
				User user=userDao.login(inputName.getText(), inputPassword.getText());
				if(user!=null) {
					UsersDashboards.displayDashboards(user);
					getTaskStates();
				
					window.close();
				}
				else {
					
				}
			}
		});
		GridPane.setConstraints(loginButton, 1, 2);
		*/
		//adding elements to grid
		//grid.getChildren().addAll(labelName, inputName, labelPass, inputPassword, loginButton);
		
		/*GridPane grid=new GridPane();
		HBox hbox1=new HBox();
		Button button1=new Button("On the first horizontal pane.");
		hbox1.getChildren().add(button1);
		
		
		HBox hbox2=new HBox();
		Button button2=new Button("On the second horizontal pane.");
		hbox2.getChildren().add(button2);
		grid.getChildren().addAll(hbox1);*/
		HBox vbox=new HBox();
		VBox vbox1=new VBox();
		//vbox1.setAlignment(Pos.BOTTOM_CENTER);
		Button button1=new Button("On the first vertical pane.");
		Button button4=new Button("On the first vertical pane.");
		vbox1.getChildren().addAll(button1);
		VBox vbox2=new VBox();
		//vbox2.setAlignment(Pos.CENTER);
		Button button2=new Button("On the second vertical pane.");
		vbox2.getChildren().add(button2);
		VBox vbox3=new VBox();
		Button button3=new Button("On the third vertical pane.");
		vbox3.getChildren().add(button3);
		//vbox3.setAlignment(Pos.TOP_CENTER);
		vbox.getChildren().addAll(vbox1, vbox2, vbox3);
		scene=new Scene(vbox, 500, 500);
		window.setScene(scene);
		window.show();
	}
	private boolean validate(TextField inputName, TextField inputPassword) {
		if(inputName.getText().trim().equals("") || inputPassword.getText().trim().equals("")) {
			if(inputName.getText().trim().equals("")) {
				inputName.setText("This field must not be empty.");
			}
			if(inputPassword.getText().trim().equals("")) {
				inputPassword.setText("This field must not be empty.");
			}
			return false;
		}
		return true;
	}
	public static void main(String[] args) {
		launch(args);
	}
	
	public ObservableList<TaskState> getTaskStates(){
		ObservableList<TaskState> taskStates=FXCollections.observableArrayList();
		Dashboard d=new Dashboard();
		d.setId(1);
		DashboardDAO dd=new DashboardDAO();
		List<TaskState> tss=dd.getTaskStates(d);
		for(TaskState ts:tss) {
			taskStates.add(ts);
		}
		for(TaskState ts:taskStates) {
			System.out.println(ts.getTitle());
		}
		return taskStates;
	}
}