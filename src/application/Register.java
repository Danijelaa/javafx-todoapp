package application;

import java.util.List;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import rs.laniteo.todoapp.dao.UserDAO;
import rs.laniteo.todoapp.model.Dashboard;
import rs.laniteo.todoapp.model.User;

public class Register {

	public static Scene register() {
		GridPane grid=new GridPane();
		//***************IZMJENA
		grid.setPadding(new Insets(15,30,15,30));
		grid.setVgap(8);
		grid.setHgap(10);
		//******************
		grid.setStyle("-fx-background-color:lightblue");
		Label labelName=new Label("First name:");
		//******************
		labelName.setStyle("-fx-font-size: 15px");
		GridPane.setConstraints(labelName, 0, 0);
		final TextField inputName=new TextField();
		inputName.setPromptText("First name");
		GridPane.setConstraints(inputName, 1, 0);
		
		Label labelLastName=new Label("Last name:");
		//********************
		labelLastName.setStyle("-fx-font-size: 15px");
		GridPane.setConstraints(labelLastName, 0, 1);
		final TextField inputLastName=new TextField();
		inputLastName.setPromptText("Last name");
		GridPane.setConstraints(inputLastName, 1, 1);
		
		Label username=new Label("Username:");
		//*******************
		username.setStyle("-fx-font-size: 15px");
		GridPane.setConstraints(username, 0, 2);
		final TextField inputUsername=new TextField();
		inputUsername.setPromptText("Username");
		GridPane.setConstraints(inputUsername, 1, 2);
		
		Label labelPass=new Label("Password:");
		//********************
		labelPass.setStyle("-fx-font-size: 15px");
		GridPane.setConstraints(labelPass, 0, 3);
		final TextField inputPassword=new TextField();
		inputPassword.setPromptText("Password");
		GridPane.setConstraints(inputPassword, 1, 3);
		
		Label labelPassRepeat=new Label("Repeat password:");
		//*************************
		labelPassRepeat.setStyle("-fx-font-size: 15px");
		GridPane.setConstraints(labelPassRepeat, 0, 4);
		final TextField inputPasswordRepeated=new TextField();
		inputPasswordRepeated.setPromptText("Repeated password");
		GridPane.setConstraints(inputPasswordRepeated, 1, 4);
		
		Button registerButton=new Button("Register");
		registerButton.setStyle("-fx-background-color:dodgerblue; -fx-text-fill:white; -fx-font-size: 13px; -fx-font-weight: bold");
		registerButton.setOnAction(
				new EventHandler<ActionEvent>() {
					
					public void handle(ActionEvent arg0) {
						boolean validated=validate(inputName.getText(), inputLastName.getText(), inputUsername.getText(), inputPassword.getText(), inputPasswordRepeated.getText());
						if(!validated) {
							return;
						}
						UserDAO userDAO=new UserDAO();
						User user=new User();
						user.setLastName(inputLastName.getText());
						user.setName(inputName.getText());
						user.setPassword(inputPassword.getText());
						user.setUsername(inputUsername.getText());
						userDAO.addUser(user);
						user=userDAO.findByUsername(inputUsername.getText());
						Login.loggedUser=user;
						Login.window.close();
						UsersDashboards.displayDashboards(user);
						
					}
				}
				/*e->{
			boolean validated=validate(inputName.getText(), inputLastName.getText(), inputUsername.getText(), inputPassword.getText(), inputPasswordRepeated.getText());
			if(!validated) {
				return;
			}
			UserDAO userDAO=new UserDAO();
			User user=new User();
			user.setLastName(inputLastName.getText());
			user.setName(inputName.getText());
			user.setPassword(inputPassword.getText());
			user.setUsername(inputUsername.getText());
			userDAO.addUser(user);
			user=userDAO.findByUsername(inputUsername.getText());
			Login.loggedUser=user;
			Login.window.close();
			UsersDashboards.displayDashboards(user);
		}*/);
		GridPane.setConstraints(registerButton, 1, 5);
		
		Label loginLabel=new Label("Already have an account?");
		GridPane.setConstraints(loginLabel, 0, 6);
		Button loginButton=new Button("Login");
		loginButton.setStyle("-fx-background-color:#f7d06e; -fx-text-fill:white; -fx-font-size: 13px; -fx-font-weight: bold");
		GridPane.setConstraints(loginButton, 1, 6);
		loginButton.setOnAction(
				new EventHandler<ActionEvent>() {
					public void handle(ActionEvent e){
						Login.window.setScene(Login.scene);
					}
				}
				/*e->Login.window.setScene(Login.scene)*/);
		grid.getChildren().addAll(labelName, inputName, labelLastName, inputLastName,username, inputUsername, labelPass, inputPassword, labelPassRepeat, inputPasswordRepeated,registerButton, loginLabel, loginButton);
		//*************IYMJENA DIMENYIJE
		Scene scene=new Scene(grid, 500, 275);
		return scene;
		
	}
	
	public static boolean validate(String name, String lastName, String username, String password, String repeatedPassword) {
		if(name.trim().equals("") || lastName.trim().equals("") || username.trim().equals("")) {
			AlerBox.display("Empty fields", "All fields must be filled.");
			return false;
		}
		if(!password.equals(repeatedPassword)) {
			AlerBox.display("", "Passwords do not match.");
			return false;
		}
		return true;
	}
}
