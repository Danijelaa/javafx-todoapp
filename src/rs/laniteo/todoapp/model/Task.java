package rs.laniteo.todoapp.model;
/**
 * @author laniteob
 * This class represents tasks of user. It represents data kept in file on server.
 */

public class Task {

	private Integer id;
	private String title;
	private String description;
	private TaskState taskState;
	
	
	/**
	 * @param id is unique identificator for instances of this class
	 * @param title is title of task
	 * @param description
	 * @param taskState
	 */
	public Task(Integer id, String title, String description, TaskState taskState) {
		super();
		this.id = id;
		this.title = title;
		this.description = description;
		this.taskState = taskState;
	}
	/**
	 * @param title is title of task
	 * @param description is description of task
	 * @param taskState is reference to TaskState instance which represents task state of task
	 */
	public Task(String title, String description, TaskState taskState) {
		super();
		this.title = title;
		this.description = description;
		this.taskState = taskState;
	}
	public Task() {
		super();
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public TaskState getTaskState() {
		return taskState;
	}
	public void setTaskState(TaskState taskState) {
		this.taskState = taskState;
	}

}
