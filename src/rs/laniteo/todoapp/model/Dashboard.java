package rs.laniteo.todoapp.model;

/**
 * @author laniteob
 * This class represents dashboard of user. It represents data kept in file on server.
 */

public class Dashboard {

	private static Long index=3L;
	private Integer id;
	private String title;
	private User user;
	
	
	public Dashboard() {
		super();
	}
	/**
	 * @param id is unique identificator of instances of this class.
	 * @param title is title of Dashboard instance.
	 * @param user is reference to instance of User class.
	 */
	public Dashboard(Integer id, String title, User user) {
		super();
		this.id = id;
		this.title = title;
		this.user = user;
	}

	/**
	 * @param title is title of Dashboard instance.
	 * @param user is reference to instance of User class.
	 */
	public Dashboard(String title, User user) {
		super();
		this.title = title;
		this.user = user;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public static Long getIndex() {
		return index;
	}
	public static void setIndex(Long index) {
		Dashboard.index = index;
	}

}
