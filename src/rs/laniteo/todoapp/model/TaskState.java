package rs.laniteo.todoapp.model;
/**
 * @author laniteob
 * This class represents task states of user's dashboard. It represents data kept in file on server.
 *
 */

public class TaskState {

	private static Long index=5L;
	private Integer id;
	private String title;
	private Dashboard dashboard;
	/**
	 * @param id is unique indenticicator of instances of this class
	 * @param title is title of task state
	 * @param dashboard is reference to instance of Dashboard where task state is
	 */
	public TaskState(Integer id, String title, Dashboard dashboard) {
		super();
		this.id = id;
		this.title = title;
		this.dashboard = dashboard;
	}
	
	/**
	 * @param title is unique indenticicator of instances of this class
	 * @param dashboard is reference to instance of Dashboard where task state is
	 */
	public TaskState(String title, Dashboard dashboard) {
		super();
		this.title = title;
		this.dashboard = dashboard;
	}

	public TaskState() {
		super();
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Dashboard getDashboard() {
		return dashboard;
	}
	public void setDashboard(Dashboard dashboard) {
		this.dashboard = dashboard;
	}

	public static Long getIndex() {
		return index;
	}

	public static void setIndex(Long index) {
		TaskState.index = index;
	}

	@Override
	public String toString() {
		return title;
	}

	
}
