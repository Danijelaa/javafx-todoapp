package rs.laniteo.todoapp.model;
/**
 * @author laniteob
 *This class represents user. It represents data kept in file on server.
 */

public class User {

	private Integer id;
	private String name;
	private String lastName;
	private String username;
	private String password;
	
	
	public User() {
		super();
	}
	
	/**
	 * @param id is unique identificator of instances of this class.
	 * @param name is name of user.
	 * @param lastName is last name of user.
	 * @param username is username of user.
	 * @param password is password of user.
	 */
	public User(Integer id, String name, String lastName, String username, String password) {
		super();
		this.id = id;
		this.name = name;
		this.lastName = lastName;
		this.username = username;
		this.password = password;
	}

	/**
	 * @param name is name of user
	 * @param lastName is last name of user
	 * @param username is username of user
	 * @param password is password of user
	 */
	public User(String name, String lastName, String username, String password) {
		super();
		//this.id = index++;
		this.name = name;
		this.lastName = lastName;
		this.username = username;
		this.password = password;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

}
