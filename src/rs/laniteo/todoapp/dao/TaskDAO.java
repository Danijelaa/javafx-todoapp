package rs.laniteo.todoapp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import rs.laniteo.todoapp.model.Task;
import rs.laniteo.todoapp.model.TaskState;
import rs.laniteo.todoapp.utils.CreateConnection;

public class TaskDAO {

	public Task findById(Integer id) {
		Task task=null;
		String query="SELECT * FROM task WHERE id="+id;
		Connection conn=null;
		conn=CreateConnection.getConnection();
		Statement st=null;
		ResultSet rs=null;
		try {
			st=conn.createStatement();
			rs=st.executeQuery(query);
			if(rs.next()) {
				String title=rs.getString(2);
				String description=rs.getString(3);
				int taskStateId=rs.getInt(4);
				TaskStateDAO taskStateDao=new TaskStateDAO();
				TaskState taskState=taskStateDao.getbyid(taskStateId);
				task=new Task(id, title, description, taskState);
			}
			
		} catch (SQLException e) {
		}finally {
			if(rs!=null) {
				try {
					rs.close();
				} catch (SQLException e) {
				}
			}
			if(st!=null) {
				try {
					st.close();
				} catch (SQLException e) {
				}
			}
		}
		
		return task;
	}

	public Integer addTask(Task task) {
		Integer id=null;
		String query="INSERT INTO `task`(`title`, `description`, `taskStateId`) VALUES(?, ?, ?)";
		Connection conn=null;
		conn=CreateConnection.getConnection();
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		try {
			pstmt=conn.prepareStatement(query);
			pstmt.setString(1, task.getTitle());
			pstmt.setString(2, task.getDescription());
			pstmt.setInt(3, task.getTaskState().getId());
			pstmt.executeUpdate();
			rs=pstmt.getGeneratedKeys();
			if(rs.next()) {
				id=rs.getInt(1);
			}
		} catch (SQLException e) {
		}finally {
			if(rs!=null) {
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if(pstmt!=null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}
		}
		return id;
	}

	public boolean deleteTask(Integer taskId) {
		String query="DELETE FROM task WHERE id="+taskId;
		Connection conn=null;
		conn=CreateConnection.getConnection();
		Statement st=null;
		try {
			st=conn.createStatement();
			st.executeUpdate(query);
		} catch (SQLException e) {
			return false;
		}finally {
			if(st!=null) {
				try {
					st.close();
				} catch (SQLException e) {
				}
			}
		}
		
		return true;	
	}

	public boolean updateTask(Task task) {
		String query="UPDATE task SET title=?, description=?, taskStateId=? WHERE id=?";
		Connection conn=null;
		conn=CreateConnection.getConnection();
		PreparedStatement pstmt=null;
		try {
			pstmt=conn.prepareStatement(query);
			pstmt.setString(1, task.getTitle());
			pstmt.setString(2, task.getDescription());
			pstmt.setInt(3, task.getTaskState().getId());
			pstmt.setInt(4, task.getId());
			pstmt.executeUpdate();
		} catch (SQLException e) {
			return false;
		}finally {
			if(pstmt!=null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}
		}
		return true;
		
	}

}
