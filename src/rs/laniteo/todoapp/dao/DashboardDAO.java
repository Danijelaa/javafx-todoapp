package rs.laniteo.todoapp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import rs.laniteo.todoapp.model.Dashboard;
import rs.laniteo.todoapp.model.TaskState;
import rs.laniteo.todoapp.model.User;
import rs.laniteo.todoapp.utils.CreateConnection;

public class DashboardDAO {

	public List<TaskState> getTaskStates(Dashboard dashboard) {
		List<TaskState> taskStates=new ArrayList<>();
		String query="SELECT * FROM taskstate ts WHERE ts.dashboardId="+dashboard.getId();
		Connection conn=null;
		conn=CreateConnection.getConnection();
		Statement st=null;
		ResultSet rs=null;
		try {
			st=conn.createStatement();
			
			rs=st.executeQuery(query);
			while(rs.next()) {
				Integer id=rs.getInt(1);
				String title=rs.getString(2);
				TaskState taskState=new TaskState(id, title, dashboard);
				taskStates.add(taskState);
			}
		} catch (SQLException e) {
		}finally {
			if(rs!=null) {
				try {
					rs.close();
				} catch (SQLException e) {
				}
				
			}
			if(st!=null) {
				try {
					st.close();
				} catch (SQLException e) {
				}
			
			}
		}
		return taskStates;
	}


	public Dashboard findByid(Integer id) {
		Dashboard dashboard=null;
		String query="SELECT  * FROM dashboard d where d.id="+id;
		Connection conn=null;
		conn=CreateConnection.getConnection();
		Statement st=null;
		ResultSet rs=null;
		try {
			st=conn.createStatement();
			rs=st.executeQuery(query);
			if(rs.next()) {
				//Integer id=rs.getInt(1);
				String title=rs.getString(2);
				Integer userId=rs.getInt(3);
				UserDAO userDao=new UserDAO();
				User user=userDao.findById(userId);
				dashboard=new Dashboard(id, title, user);
			}
		} catch (SQLException e) {
		}finally {
			if(rs!=null) {
				try {
					rs.close();
				} catch (SQLException e) {
				}
				
			}
			if(st!=null) {
				try {
					st.close();
				} catch (SQLException e) {
				}
			}
		}
		
		return dashboard;
	}

	public Integer addDashboard(Dashboard dashboard) {
		String query="INSERT INTO `dashboard`(`title`, `userId`) VALUES(?, ?)";
		Connection conn=null;
		conn=CreateConnection.getConnection();
		PreparedStatement pstmt=null;
		Integer id=null;
		ResultSet rs=null;
			try {
				pstmt=conn.prepareStatement(query);
				int index=1;
				pstmt.setString(index++, dashboard.getTitle());
				pstmt.setInt(index++, dashboard.getUser().getId());
				pstmt.executeUpdate();
				rs=pstmt.getGeneratedKeys();
				if(rs.next()) {
					id=rs.getInt(1);
				}
			} catch (SQLException e) {
			}finally {
				if(rs!=null) {
					try {
						rs.close();
					} catch (SQLException e) {
					}
				}
				if(pstmt!=null) {
					try {
						pstmt.close();
					} catch (SQLException e) {
					}
				}
			}
			return id;
	}


	public boolean updateDashboard(Dashboard dashboard) {
		String query="UPDATE dashboard SET title=? WHERE id=?";
		Connection conn=null;
		conn=CreateConnection.getConnection();
		PreparedStatement pstmt=null;
			try {
				pstmt=conn.prepareStatement(query);
				int index=1;
				pstmt.setString(index++, dashboard.getTitle());
				pstmt.setInt(index++, dashboard.getId());
				pstmt.executeUpdate();
			} catch (SQLException e) {
				return false;
			}finally {
				if(pstmt!=null) {
					try {
						pstmt.close();
					} catch (SQLException e) {
					}
				}
			}
		return true;
	}

	public boolean deleteDashboard(Integer id) {
		String foreignKeysOn="PRAGMA foreign_keys = ON";
		String query="DELETE FROM  dashboard WHERE id="+id;
		Connection conn=null;
		conn=CreateConnection.getConnection();
		Statement st=null;
			try {
				st=conn.createStatement();
				st.execute(foreignKeysOn);
				st.executeUpdate(query);
			} catch (SQLException e) {
				return false;
			}finally {
				if(st!=null) {
					try {
						st.close();
					} catch (SQLException e) {
					}
				}
			}
		
		return true;		
	}
/*
	public DashboardDTOAllData allData(Integer id) {
	
		Dashboard dashboard=findByid(id);
		if(dashboard==null) {
			return null;
		}
		DashboardDTOAllData dashboardDto=new DashboardDTOAllData();
		dashboardDto.setId(dashboard.getId());
		dashboardDto.setTitle(dashboard.getTitle());
		
		List<TaskStateDTOAlldata> taskStatesDto=new ArrayList<>();
		List<TaskState> taskStates=getTaskStates(dashboard);
		
		for(TaskState ts:taskStates) {
			TaskStateDTOAlldata taskStateDto=new TaskStateDTOAlldata();
			taskStateDto.setId(ts.getId());
			taskStateDto.setTitle(ts.getTitle());
			taskStatesDto.add(taskStateDto);
			
			List<TaskDTOAllData> tasksDto=new ArrayList<>();
			List<Task> tasks=taskStateSErvice.getTasks(ts);
			for(Task t:tasks) {
				TaskDTOAllData taskDto=new TaskDTOAllData();
				taskDto.setDescription(t.getDescription());
				taskDto.setId(t.getId());
				taskDto.setTitle(t.getTitle());
				tasksDto.add(taskDto);
			}
			taskStateDto.setTasks(tasksDto);
		}
		dashboardDto.setTaskstates(taskStatesDto);
		return dashboardDto;
	}*/

}
