package rs.laniteo.todoapp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import rs.laniteo.todoapp.model.Dashboard;
import rs.laniteo.todoapp.model.User;
import rs.laniteo.todoapp.utils.CreateConnection;

public class UserDAO {

public User login(String username, String password) {
		
		User user=null;
		
		String query="SELECT * FROM user WHERE user.username LIKE ? AND user.password LIKE ?";
		Connection conn=null;
		conn=CreateConnection.getConnection();
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		try {
			pstmt=conn.prepareStatement(query);
			pstmt.setString(1, username);
			pstmt.setString(2, password);
			
			rs=pstmt.executeQuery();
			if(rs.next()) {
				Integer id=rs.getInt(1);
				String name=rs.getString(2);
				String lastName=rs.getString(3);
				user=new User(id, name, lastName, username, password);
			}
		} catch (SQLException e) {
		}finally {
			if(rs!=null) {
				try {
					rs.close();
				} catch (SQLException e) {
				}
				
			}
			if(pstmt!=null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}
		}
		return user;

	}

	public List<Dashboard> getUsersDashboards(User user) {
		List<Dashboard> dashboards=new ArrayList<>();
		String query="SELECT * FROM dashboard d WHERE d.userId="+user.getId();
		Connection conn=null;
		conn=CreateConnection.getConnection();
		Statement st=null;
		ResultSet rs=null;
		try {
			st=conn.createStatement();
			rs=st.executeQuery(query);
			while(rs.next()) {
				Integer id=rs.getInt(1);
				String title=rs.getString(2);
				Dashboard dashboard=new Dashboard(id, title, user);
				dashboards.add(dashboard);
			}
		} catch (SQLException e) {
		}finally {
			if(rs!=null) {
				try {
					rs.close();
				} catch (SQLException e) {
				}
				
			}
			if(st!=null) {
				try {
					st.close();
				} catch (SQLException e) {
				}
			}
		}
		return dashboards;
	}
	
	public User findById(Integer userId) {
		User user=null;
		String query="SELECT * FROM user u WHERE u.id="+userId;
		Connection conn=null;
		conn=CreateConnection.getConnection();
		Statement st=null;
		ResultSet rs=null;
		try {
			st=conn.createStatement();
			rs=st.executeQuery(query);
			if(rs.next()) {
				String name=rs.getString(2);
				String lastName=rs.getString(3);
				String username=rs.getString(4);
				String password=rs.getString(5);
				user=new User(userId, name, lastName, username, password);
				
			}
		} catch (SQLException e) {
		}finally {
			if(rs!=null) {
				try {
					rs.close();
				} catch (SQLException e) {
				}
				
			}
			if(st!=null) {
				try {
					st.close();
				} catch (SQLException e) {
				}
			}
		}
		return user;
	}
	
	public User findByUsername(String username) {
		User user=null;
		String query="SELECT * FROM user WHERE user.username LIKE ?";
		Connection conn=null;
		conn=CreateConnection.getConnection();
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		try {
			pstmt=conn.prepareStatement(query);
			pstmt.setString(1, username);
			
			rs=pstmt.executeQuery();
			if(rs.next()) {
				Integer id=rs.getInt(1);
				String name=rs.getString(2);
				String lastName=rs.getString(3);
				String password=rs.getString(5);
				user=new User(id, name, lastName, username, password);
			}
		} catch (SQLException e) {
		}finally {
			if(rs!=null) {
				try {
					rs.close();
				} catch (SQLException e) {
				}
				
			}
			if(pstmt!=null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}
		}
		return user;
	}
	
	public User addUser(User user) {
		String query="INSERT INTO `user`(`name`, `lastName`, `username`, `password`) VALUES(?, ?, ?, ?)";
		Connection conn=null;
		conn=CreateConnection.getConnection();
		PreparedStatement pstmt=null;
			try {
				pstmt=conn.prepareStatement(query);
				int index=1;
				pstmt.setString(index++, user.getName());
				pstmt.setString(index++, user.getLastName());
				pstmt.setString(index++, user.getUsername());
				pstmt.setString(index++, user.getPassword());
				pstmt.executeUpdate();
			} catch (SQLException e) {
			}finally {
				if(pstmt!=null) {
					try {
						pstmt.close();
					} catch (SQLException e) {
					}
				}
			}
		
		return user;
	}

}
