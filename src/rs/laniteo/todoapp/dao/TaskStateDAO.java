package rs.laniteo.todoapp.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import rs.laniteo.todoapp.model.Dashboard;
import rs.laniteo.todoapp.model.Task;
import rs.laniteo.todoapp.model.TaskState;
import rs.laniteo.todoapp.utils.CreateConnection;

public class TaskStateDAO {

	public List<Task> getTasks(TaskState taskState) {
		List<Task> tasks=new ArrayList<>();
		Connection conn=null;
		conn=CreateConnection.getConnection();
		String query="SELECT * FROM task WHERE task.taskStateId="+taskState.getId();
		Statement st=null;
		ResultSet rs=null;
		try {
			st=conn.createStatement();
			rs=st.executeQuery(query);
			
			while(rs.next()) {
				int id=rs.getInt(1);
				String title=rs.getString(2);
				String description=rs.getString(3);
				Task task=new Task(id, title, description, taskState);
				tasks.add(task);
			}
		} catch (SQLException e) {
		}finally {
			if(rs!=null) {
				try {
					rs.close();
				} catch (SQLException e) {
				}
			}
			if(st!=null) {
				try {
					st.close();
				} catch (SQLException e) {
				}
			}
		}
		
		return tasks;
	}

	public TaskState getbyid(Integer id) {
		TaskState taskState=null;
		String query="SELECT * FROM taskstate ts WHERE ts.id="+id;
		Connection conn=null;
		conn=CreateConnection.getConnection();
		Statement st=null;
		ResultSet rs=null;
		try {
			st=conn.createStatement();
			rs=st.executeQuery(query);
			if(rs.next()) {
				String title=rs.getString(2);
				Integer dashboardId=rs.getInt(3);
				DashboardDAO dashboardDao=new DashboardDAO();
				Dashboard dashboard=dashboardDao.findByid(dashboardId);
				//System.out.println("dashboard id: "+dashboard.getId());
				taskState=new TaskState(id, title, dashboard);
			}
		} catch (SQLException e) {
		}finally {
			if(rs!=null) {
				try {
					rs.close();
				} catch (SQLException e) {
				}
				
			}
			if(st!=null) {
				try {
					st.close();
				} catch (SQLException e) {
				}
			}
		}
		return taskState;
	}

	public Integer addTaskState(TaskState taskState) {
		Integer id=null;
		String query="INSERT INTO `taskstate`(`title`, `dashboardId`) VALUES(?, ?)";
		Connection conn=null;
		conn=CreateConnection.getConnection();
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		try {
			pstmt=conn.prepareStatement(query);
			pstmt.setString(1, taskState.getTitle());
			pstmt.setInt(2, taskState.getDashboard().getId());
			pstmt.executeUpdate();
			rs=pstmt.getGeneratedKeys();
			if(rs.next()) {
				id=rs.getInt(1);
			}
		} catch (SQLException e) {
		}finally{
			if(rs!=null) {
				try {
					rs.close();
				} catch (SQLException e) {
				}
			}
			if(pstmt!=null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}
		}
		return id;
	}

	public boolean updateTaskState(TaskState taskState) {
		String query="UPDATE taskstate SET title=? WHERE id=?";
		Connection conn=null;
		conn=CreateConnection.getConnection();
		PreparedStatement pstmt=null;
		try {
			pstmt=conn.prepareStatement(query);
			pstmt.setString(1, taskState.getTitle());
			pstmt.setInt(2, taskState.getId());
			pstmt.executeUpdate();
		} catch (SQLException e) {
			return false;
		}finally{
			if(pstmt!=null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
				}
			}
		}
		
		return true;
	}

	public boolean deleteTaskState(Integer taskStateId) {
		String foreignKeysOn="PRAGMA foreign_keys = ON";
		String query="DELETE FROM taskstate WHERE id="+taskStateId;
		Connection conn=CreateConnection.getConnection();
		Statement st=null;
		try {
			st=conn.createStatement();
			st.execute(foreignKeysOn);
			st.executeUpdate(query);
		} catch (SQLException e) {
			return false;
		}
		return true;		
	}

}
